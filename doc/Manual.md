Manual:

    How to run the program:
        First, there will be a menu where you must enter C in order to create a deck, or X to exit
        Then, there will be a menu where you must input the card size (N), max number for bingo cards (M), and number
        of cards
            Note:
                Card size (N) must be between 3 and 15
                Max number (M) must be between [2*N*N] and [4*N*N]
                Number of cars must be between 3 and 10000
        Then, a deck menu will dislay that has the options:
            Enter 'P' to print a card to the screen
                Specify which card to print, using its id number
            Enter 'D' to display the whole deck to the screen
            Enter 'S' to save the deck to a file
                Specify the filename
            Enter 'X' to quit
    Errors:
        If any input is incorrect, the menu will repeat itself, and an error message will be displayed
        