Peer review for Clayton Holdstock's UML Diagram:

        Clayton, I enjoy the simplicity of your diagram and how easy it is to follow. Though, part of this simplicity is
    due to the lack of details that this diagram could use. If you reference Erik’s PowerPoint titles “Objects and
    Classes” and “More About Classes” especially, we can see it is helpful to specify how the classes interact with each
    other that is more than just simply arrows. I admired the specification of the data member return type but think it
    would be improved if you added that return type to your methods as well, if they have one. For example, the print
    methods may not return anything, but the get methods likely have a return type. On top of what the method’s return
    type is, it would be helpful for the viewer if you added the parameters of your methods and their return types, as
    that would add some detail to this diagram. Even though in python it is obvious which methods and data members may
    be private, it would likely be a good idea to add some sort of extra specification in whether they are or not, like
    how Erik used + and -, for example. Another large thing I noticed is the absence of the “__m_” in the header data
    member, which I understand Python ignores the double underscore, but I was questioning why you disregarded the “m_”
    as it is part of the variable name, if you were attempting to write out the whole name that is. Finally, I would
    recommend adding more data members that are involved in these classes. With all that said, it is a great first draft
    that can be easily improved.
