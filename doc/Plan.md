Software Development Plan:

    1. Requirements:
        This program uses classes that work together to create a deck of bingo cards. Amount, size of cards, and
        the range of numbers on the cards. The size of this bingo card is NxN, where N can be 3-15. The numbers on
        the cards come from a set of numbers that can range from 1-M, where M is a user-specified max between 2*N*N
        and 4*N*N. Finally, the number of cards in a deck is 3-10000. Every card is assigned a unique integer, so it
        may be retrieved. 

    2. Design:
        Program must contain Deck.py class that can:
            Construct a deck object, where user specifies:
                Size of cards
                Number of cards
                And maximum numbers in a bingo set
            A method to print a specific card
            A method to print the entire deck
            A method to send the deck to a file of the user's choice
        It is recommended that Deck.py is not the only class
        Every card is assigned a unique integer identifier
            Should also be able to retrieve this card based on identifier
        No duplicate bingo numbers on a card
        The previous deck is lost when another is created
        Once deck object created, it does not change
        Output:
            A deck of:
                Cards ranging from 3-10000
                Each card has a set of bingo numbers, none the same as another
                Each card being a square, NxN

    3. Implementation:
        This program is located in src/, and is run from there
        User is required to enter input for bingo cards, and what to do with them
        Unit tests will be created to verify code

    4. Verification:
        Unit tests needed to verify code
        Test one:
            User entered 2 as the deck size, what happens...?
                Error message is displayed to enter valid input, question is repeated
        Test two:
            Are all bingo numbers different on a card when user specifies a card size of 4x4...?
                Yes, they are
