Reviews done for my on my UML Diagram:

    Clayton Holdstock's Review:
        At first glance your UML looks way good! It is well organized and seems to flow well from one area into the next.
    Your classes and methods are well defined and show the correct inputs and outputs. I like the clarification that
    your UML has with the <uses> tags. I think that if I had any input it would be to clarify those a little more like,
    <is part of> or descriptions like that! Another thing that you could look into is the UserInterface class and the
    assigned outputs. For example, the creatDeck() function should create and return a Deck object. Another great thing
    you’ve done is define the visibility of the methods using, (+ and -). Though in python we don’t have private classes
    it is still good to know what methods can be changed and which are not to be used by the user. That is a great
    touch. Other than that I think your UML has set you up for implementation and a successful program. It looks really
    good to me and I think you’ve done a great job on it!
    
    Rebecca Zitting's Review:
        Isaac, your UML diagram was incredibly simple and easy to follow. Your arrows make sense in the context of the
    program, and I like how you used the <creates> and <uses> terminology that Erik used in his PowerPoints. You've done
    a great job at declaring the types of the specific class methods. I would say just to keep going with that approach
    and make sure to define the methods that will return none, as well, just to be specific. This is up to you in the
    case of personal preference, but it may be beneficial to you to specify the name of the type that is being returned
    for those methods, just to keep things straight in your head in addition to the types that you have declared. In 
    regards to the arrow between Deck and NumberSet, I'm glad that you put that there. I did not immediately realize
    that that was associated with each other. I will have to add that to my UML diagram! This is a small comment, but I
    do not immediately see a purpose for the <Uses> tag in your numberSet class; I believe that that might be a typo, so
    make sure to delete that for your final draft or specify what its use is depending on if it has a purpose there or
    not. Otherwise, your diagram looks great, and it is clear to see you have a great understanding of how the program
    will work from your design phase.
    