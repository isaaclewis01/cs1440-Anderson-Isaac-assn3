Peer review for Rebecca Zitting's UML Diagram:

        Becca, your UML diagram is a wonderful first draft. I admire where you put the question marks, labeling where
    you are not sure of a certain value. With these, I would make sure you get back to them so that your diagram
    looks complete. I enjoy the user-friendly readability, and how easy the arrows are to follow. I also think the
    specific labeling of the arrows that describe how the classes interact with one another. The addition of parameters
    to your methods is a good touch, although following Erik's PowerPoint titled "More About Classes," we can see that
    he specifies the return type of even these parameters, which I think would be a good inclusion. I noticed that at
    times you specified the return type of a data member or method was specified with the ':' character while at other
    times was specified with the '-' which I think is a little confusing to follow and would be improved with the 
    addition of consistency. I admire the adjustment of the rectangles that the describe the classes, as they result
    in readability and even a hint of a personal touch. Something that I thought may be good to add would be more arrows
    coming from a class like "Deck" that many other classes use. It looks nice to have one chain of classes, but I
    think that it is more interconnected and complex than a simple connection of arrows and boxes. Overall though, this
    diagram is a great first draft, as it is so close to being a final draft that little adjustments are needed. A great
    looking UML, if you ask me.
    