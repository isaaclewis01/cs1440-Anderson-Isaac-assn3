| Dates | Events |
|-------|--------|
| 2/26/2020 | Cloned repository |
| 2/27/2020 | Had no time to work |
| 2/28/2020 | Started UML Diagram |
| 2/29/2020 | Worked on UML |
| 3/1/2020 | Had no time to work |
| 3/2/2020 | Worked on UML |
| 3/3/2020 | Finished first draft of UML |
| 3/4/2020 | Finished design phase |
| 3/5/2020 | Worked on program, UserInterface.py especially |
| 3/6/2020 | Finished assignment base classes |
| 3/7/2020 | Finished program: updated documentation, added tests |
