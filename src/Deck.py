import sys

import Card
import NumberSet

class Deck:
    def __init__(self, cardSize, cardCount, numberMax):
        """Deck constructor"""
        self.__m_cardCount = int(cardCount)
        self.__m_cardSize = int(cardSize)
        self.__m_numberMax = int(numberMax)
        self.__m_cards = []

    def __setCards(self):
        for i in range(self.__m_cardCount):
            numberSet = NumberSet.NumberSet(self.__m_cardSize, self.__m_numberMax).numberSet()
            cardObj = Card.Card(i, numberSet, self.__m_cardSize)
            self.__m_cards.append(cardObj)

    def getCardCount(self):
        """Return an integer: the number of cards in this deck"""
        return self.__m_cardCount

    def getCard(self, n):
        """Return card N from the deck"""
        self.__setCards()
        card = None
        n -= 1
        if 0 <= n < self.getCardCount():
            card = self.__m_cards[n]
        return card

    def getCardArray(self):
        return self.__m_cards

    def print(self, file=sys.stdout, idx=None):
        """void function: Print cards from the Deck

        If an index is given, print only that card.
        Otherwise, print each card in the Deck
        """
        idCounter = 1
        if file == sys.stdout:
            if idx is None:
                for idx in range(1, self.__m_cardCount + 1):
                    c = self.getCard(idx)
                    print("Card #" + str(idCounter))
                    c.print()
                    idCounter += 1
                print('')
            else:
                print("Card #" + str(idx))
                self.getCard(idx).print()
        else:
            for idx in range(1, self.__m_cardCount + 1):
                c = self.getCard(idx)
                file.write("Card #" + str(idCounter) + "\n" + c.getCardString() + "\n")
                idCounter += 1
