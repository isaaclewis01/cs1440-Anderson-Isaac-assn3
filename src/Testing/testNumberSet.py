# Tests the NumberSet Class

import unittest
import NumberSet

class TestNumberSet(unittest.TestCase):
    def setUp(self):
        self.numberSet = NumberSet.NumberSet(18, 333)
        self.numberSet1 = NumberSet.NumberSet(0, 0)

    def test_getSize(self):
        self.assertIsNotNone(self.numberSet)
        self.assertIsNotNone(self.numberSet1)
        self.assertEqual(self.numberSet.getSize(), 324)
        self.assertEqual(self.numberSet1.getSize(), 0)

    def test_getMax(self):
        self.assertIsNotNone(self.numberSet)
        self.assertIsNotNone(self.numberSet1)
        self.assertEqual(self.numberSet.getMax(), 333)
        self.assertEqual(self.numberSet1.getMax(), 0)

    def test_numberSet(self):
        self.assertIsNotNone(self.numberSet)
        self.assertIsNotNone(self.numberSet1)
        self.assertEqual(len(self.numberSet.numberSet()), 324)
        self.assertEqual(len(self.numberSet1.numberSet()), 0)


if __name__ == '__main__':
    unittest.main()