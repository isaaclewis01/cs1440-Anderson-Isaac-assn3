import unittest
import Card
import NumberSet

class TestCard(unittest.TestCase):
    def setUp(self):
        self.card = Card.Card(1, NumberSet.NumberSet(4, 48), 4)
        self.card1 = Card.Card(2, NumberSet.NumberSet(15, 450), 15)


    def test_getSize(self):
        self.assertIsNotNone(self.card)
        self.assertIsNotNone(self.card1)
        self.assertEqual(self.card.getSize(), 4)
        self.assertEqual(self.card1.getSize(), 15)

    def test_getID(self):
        self.assertIsNotNone(self.card)
        self.assertIsNotNone(self.card1)
        self.assertEqual(self.card.getId(), 1)
        self.assertEqual(self.card1.getId(), 2)



if __name__ == '__main__':
    unittest.main()

