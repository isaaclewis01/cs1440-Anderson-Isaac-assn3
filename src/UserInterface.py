import Deck
import Menu


class UserInterface:
    def __init__(self):
        self.__m_cardCount = 0
        self.__m_cardSize = 0
        self.__m_numberMax = 0
        self.__m_currentDeck = None

    def run(self):
        """Present the main menu to the user and repeatedly prompt for a valid command"""
        print("Welcome to the Bingo! Deck Generator\n")
        menu = Menu.Menu("Main")
        menu.addOption("C", "Create a new deck")

        keepGoing = True
        while keepGoing:
            command = menu.show()
            if command == "C":
                self.__createDeck()
                keepGoing = False
            elif command == "X":
                keepGoing = False

    def __createDeck(self):
        """Command to create a new Deck"""
        # Get the user to specify the card size, max number, and number of cards
        print("\nSpecify:")
        errorMsg = "\n-Enter valid input-\n"
        again = True
        while again:
            self.__m_cardSize = input("\tCard size (N) within the range [3, 15], where the card is NxN: ")
            if self.__m_cardSize.isdigit():
                if 3 <= int(self.__m_cardSize) <= 15:
                    again = False
                else:
                    again = True
                    print(errorMsg)
            else:
                again = True
                print(errorMsg)
        again = True
        while again:
            self.__m_numberMax = input("\tMax number in bingo-set within the range [" +
                    str(2 * (int(self.__m_cardSize) ** 2)) + ", " + str(4 * (int(self.__m_cardSize) ** 2)) + "]: ")
            if self.__m_numberMax.isdigit():
                if (2 * (int(self.__m_cardSize) ** 2)) <= int(self.__m_numberMax) <= (4 * (int(self.__m_cardSize) ** 2)):
                    again = False
                else:
                    again = True
                    print(errorMsg)
            else:
                again = True
                print(errorMsg)
        again = True
        while again:
            self.__m_cardCount = input("\tNumber of cards in a deck within the range [3, 10000]: ")
            if self.__m_cardCount.isdigit():
                if 3 <= int(self.__m_cardCount) <= 10000:
                    again = False
                else:
                    again = True
                    print(errorMsg)
            else:
                again = True
                print(errorMsg)
        self.__m_currentDeck = Deck.Deck(self.__m_cardSize, self.__m_cardCount, self.__m_numberMax)
        self.__deckMenu()

    def __deckMenu(self):
        """Present the deck menu to user until a valid selection is chosen"""
        menu = Menu.Menu("Deck")
        menu.addOption("P", "Print a card to the screen")
        menu.addOption("D", "Display the whole deck to the screen")
        menu.addOption("S", "Save the whole deck to a file")

        keepGoing = True
        while keepGoing:
            command = menu.show()
            if command == "P":
                self.__printCard()
            elif command == "D":
                print()
                self.__m_currentDeck.print()
            elif command == "S":
                self.__saveDeck()
            elif command == "X":
                keepGoing = False

    def __printCard(self):
        """Command to print a single card"""
        cardToPrint = input("Enter card number: ")
        if cardToPrint.isdigit():
            if int(self.__m_cardCount) >= int(cardToPrint) > 0:
                print()
                self.__m_currentDeck.print(idx=int(cardToPrint))
            else:
                print("-Enter valid input-")
        else:
            print("-Enter valid input-")

    def __saveDeck(self):
        """Command to save a deck to a file"""
        fileName = input("Enter output file name: ")
        if fileName != "":
            outputStream = open(fileName, 'w')
            self.__m_currentDeck.print(outputStream)
            outputStream.close()
            print("Done!")
        else:
            print("\n-Enter valid input-")
