import random


class NumberSet:
    def __init__(self, size, max):
        """NumberSet constructor"""
        self.__m_size = int(size)
        self.__m_max = int(max)

    def numberSet(self):
        numberSet = [0] * self.getSize()
        testEqual = True

        for i in range(self.getSize()):
            randNum = random.randint(1, self.__m_max)
            numberSet[i] = randNum

        while testEqual:
            doubleCount = 0
            for i in range(self.getSize()):
                if numberSet.count(numberSet[i]) == 1:
                    doubleCount += 1
                else:
                    numberSet[i] = random.randint(1, self.__m_max)
            testEqual = doubleCount != self.getSize()

        return numberSet

    def getSize(self):
        """Return an integer: the size of the NumberSet"""
        if self.__m_size % 2 == 0:
            return self.__m_size * self.__m_size
        else:
            return self.__m_size * self.__m_size - 1

    def getMax(self):
        """Return an integer: the max number that a number in numberSet can be"""
        return self.__m_max
