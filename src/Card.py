class Card:
    def __init__(self, idnum, numberSet, size):
        """Card constructor"""
        self.__m_size = int(size)
        self.__m_idnum = idnum
        self.__m_numberSet = numberSet
        self.__m_cardString = ''

    def getSize(self):
        """Return an integer: the size of one dimension of the card.
        A 3x3 card will return 3, a 5x5 card will return 5, etc.
        """
        return self.__m_size

    def getId(self):
        return self.__m_idnum

    def __setCardString(self):
        # Sets card string
        self.__m_cardString = ''

        # If the card has even dimensions
        if self.__m_size % 2 == 0:
            for j in range(self.__m_size):
                for i in range(self.__m_size):
                    self.__m_cardString += "+-----"
                self.__m_cardString += "+"
                self.__m_cardString += "\n"
                for i in range(self.__m_size):
                    self.__m_cardString += ("|" + format(str(self.__m_numberSet[i + (j * self.__m_size)]), "^5"))
                self.__m_cardString += "|"
                self.__m_cardString += "\n"
            for i in range(self.__m_size):
                self.__m_cardString += "+-----"
            self.__m_cardString += "+"

            self.__m_cardString += "\n"

            #print(self.__m_cardString)

        # If the card has odd dimensions
        else:
            # First half of rows
            for j in range(int(self.__m_size / 2)):
                for i in range(self.__m_size):
                    self.__m_cardString += "+-----"
                self.__m_cardString += "+"
                self.__m_cardString += "\n"
                for i in range(self.__m_size):
                    self.__m_cardString += ("|" + format(str(self.__m_numberSet[i + (j * self.__m_size)]), "^5"))
                self.__m_cardString += "|"
                self.__m_cardString += "\n"
            for i in range(self.__m_size):
                self.__m_cardString += "+-----"
            self.__m_cardString += "+"
            self.__m_cardString += "\n"
            # Middle row
            for i in range(int(self.__m_size / 2)):
                self.__m_cardString += "|" + format(str(self.__m_numberSet[i + self.__m_size * (int(self.__m_size / 2))]), "^5")
            # Print free space
            self.__m_cardString += ("|" + format("FREE!", "^5"))
            for i in range(int(self.__m_size / 2)):
                self.__m_cardString += ("|" + format(str(self.__m_numberSet[i + self.__m_size * (int(self.__m_size / 2))
                                                          + (int(self.__m_size / 2))]), "^5"))
            self.__m_cardString += "|"
            self.__m_cardString += "\n"
            # Second half of rows
            for j in range(int(self.__m_size / 2)):
                for i in range(self.__m_size):
                    self.__m_cardString += "+-----"
                self.__m_cardString += "+"
                self.__m_cardString += "\n"
                for i in range(self.__m_size):
                    self.__m_cardString += "|" + format(str(self.__m_numberSet[(self.__m_size * (int(self.__m_size / 2) + 1))
                                                                      - 1 + i + (j * self.__m_size)]), "^5")
                self.__m_cardString += "|"
                self.__m_cardString += "\n"
            for i in range(self.__m_size):
                self.__m_cardString += "+-----"
            self.__m_cardString += "+"

            self.__m_cardString += "\n"

            #print(self.__m_cardString)

    def getCardString(self):
        self.__setCardString()
        return self.__m_cardString

    def print(self):
        """void function:
                Prints a card to the screen or to an open file object"""
        self.__setCardString()
        print(self.__m_cardString)